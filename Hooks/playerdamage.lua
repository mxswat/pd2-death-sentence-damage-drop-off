-- Oh, hello welcome to my code
-- here some stats
-- 22.5 is heavy damage and 6.75 is light swat dmg number to be multiplied by 10, 225 and 67.5
-- will be for light 75 at point blank
-- will be for heavy 100 at point blank
-- thanks Offyerrocker for crackdown damage infos

local old_PlayerDamage_damage_bullet = PlayerDamage.damage_bullet
log("MxPlayerDamage")
function PlayerDamage:damage_bullet(attack_data)
    local show_log = false
    if not self:_chk_can_take_dmg() then
        return
    end
    local distance = mvector3.distance(self._unit:position(), attack_data.attacker_unit:position()) / 100
    local new_damage = attack_data.damage
    if attack_data.damage == 22.5 then
        new_damage = self:heavy_swat_damage(distance)
    end
    if attack_data.damage == 6.75 then
        new_damage = self:light_swat_damage(distance)
    end
    if show_log then -- log disabler on enabler
        log(
            "ill be damaged from" ..
                tostring(distance) .. "by " .. tostring(new_damage) .. " instead of " .. tostring(attack_data.damage)
        )
    end
    attack_data.damage = new_damage
    old_PlayerDamage_damage_bullet(self, attack_data)
end

function PlayerDamage:heavy_swat_damage(distance)
    local new_damage = 0
    if distance <= 5 then
        new_damage = 100
    elseif distance <= 10 then
        new_damage = 75
    elseif distance <= 20 then
        new_damage = 70
    elseif distance <= 30 then
        new_damage = 68
    elseif distance <= 60 then
        new_damage = 52
    else -- must be over 45 than
        new_damage = 48
    end
    return new_damage / 10
end

function PlayerDamage:light_swat_damage(distance)
    local new_damage = 0
    if distance <= 5 then
        new_damage = 75
    elseif distance <= 10 then
        new_damage = 60
    elseif distance <= 20 then
        new_damage = 48
    elseif distance <= 30 then
        new_damage = 42
    elseif distance <= 45 then
        new_damage = 37
    else -- must be over 45 than
        new_damage = 30
    end
    return new_damage / 10
end

-- ene_zeal_swat Idstring(@ID9909f112cf60d6ca@)
-- ene_zeal_swat_heavy Idstring(@IDc59df88e5d1b14ee@)
-- attack_data Idstring(@ID399cf7248c751fb8@)
-- attack_data.damage 22.5

-- attack_data Idstring(@ID116a8a6f2405e15f@)
-- attack_data.damage 6.75
